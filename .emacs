;;-------------------------------------
;; set env variable
;; add emax bins to PATH and exec-path
;;
(defvar emax-root (concat (expand-file-name "~") "/emax"))
(defvar emax-bin (concat emax-root "/bin"))
(defvar emax-bin64 (concat emax-root "/bin64"))
(defvar emax-mingw64 (concat emax-root "/mingw64/bin"))
(defvar cygwin64-bin "c:/Cygwin64/bin")
(defvar chp-exe (concat  (expand-file-name "~") "/chp_exe"))

(setq exec-path (cons emax-bin exec-path))
(setenv "PATH" (concat emax-bin ";" (getenv "PATH")))

(setq exec-path (cons emax-bin64 exec-path))
(setenv "PATH" (concat emax-bin64 ";" (getenv "PATH")))

(setq exec-path (cons cygwin64-bin exec-path))
(setenv "PATH" (concat cygwin64-bin ";" (getenv "PATH")))

(setq exec-path (cons chp-exe exec-path))
(setenv "PATH" (concat chp-exe ";" (getenv "PATH")))

(setq exec-path (cons emax-mingw64 exec-path))
(setenv "PATH" (concat emax-mingw64 ";" (getenv "PATH")))
(setenv "PATH" (concat
                emax-mingw64 "/bin" ";"
                emax-mingw64 "/usr/bin" ";"  (getenv "PATH")))

(dolist (dir '("~/emax/" "~/emax/bin/" "~/emax/bin64/" "~/emax/mingw64/bin/" "~/emax/lisp/" "~/emax/elpa/"))
  (add-to-list 'load-path dir))

;; ------------------------------------
;; use CN elpa sites
;;
(require 'package)
(setq package-archives '(("gnu"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
                         ("melpa" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/")
                         ("ox-odt" . "https://kjambunathan.github.io/elpa/")))

(setq package-user-dir "c:/emax64/.emacs.d/elpa")
(package-initialize)


(server-start)

;;
;;
;;

(setq gc-cons-threshold 50000000)

;; ---------------------------------
;; remove backup-directory-alist
;;
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; ----------------------------------
;; backup in one place. flat, no tree structure
;;
(setq make-backup-files nil)
(setq auto-save-default nil)
(setq create-lockfiles nil)

;; ----------------------------------
;; * turn off initial start-up screen
;; * menu
;; * turn off scroll bar
;; * turn on time display
;; * turn on line number
;; * no blink corsor
;; * turn off bell
;;
(setq inhibit-splash-screen t
      initial-scratch-message nil
      initial-major-mode 'org-mode)

(menu-bar-mode 1)
(tool-bar-mode -1)
(toggle-scroll-bar -1)

(display-time-mode 1)
(global-display-line-numbers-mode 1)
(setq display-line-numbers-width 5)
(setq display-line-numbers-width-start t)
(blink-cursor-mode 0)
(setq visible-bell 1)

;; -------------------------------------
;; change yes-or-no-p to y-or-n-p
;;
(fset 'yes-or-no-p 'y-or-n-p)

;; ----------------------------------------
;;use standard keys for undo cut copy paste
;;
(cua-mode 1)

;; ----------------------------------------
;; auto insert closing bracket, and highlight brackets
;;
(electric-pair-mode 1)
(progn
  (show-paren-mode 1)
  (setq show-paren-style 'parenthesis)
  )

;; ---------------------------------------
;; make indetation commands use space only
;;
(setq-default indent-tabs-mode nil)

;; ---------------------------------------
;; UTF-8 as default encoding
;;
(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

;; ----------------------------------------
;; when a file is updated outside emacs, make it update if it's already opened in emacs
;;
(global-auto-revert-mode 1)

;; ---------------------------------------
;; set dafault directory path/to/MET(ME TODO files)
;;
(setq default-directory "D:/Project/0UNotes/0gtd")

;; ---------------------------------------
;; enable recent files and bind to f7
;; for dird view, refer to dired-sidebar
;;
(require 'recentf)
(recentf-mode 1)
(global-set-key (kbd "<f7>") 'recentf-open-files)


;; ---------------------------------------
;; fonts set
;; *DO NOT* use size parameter for fontset, which is
;; not compatible with Hi-DPI in Window 10 
;;
(when (member "Consolas" (font-family-list))
  (set-face-attribute 'default nil :font "Consolas"))
;; for Chinese 中文
(dolist (charset '(kana han symbol cjk-misc bopomofo))
  (set-fontset-font (frame-parameter nil 'font)
                    charset
                    (font-spec :family "WenQuanYi Micro Hei")))
;;(add-to-list 'default-frame-alist '(font . "Noto Mono"))

;; set line spacing
(setq-default line-spacing 0.3)

;; --------------------------------------
;; window movement keybindings
;;
(windmove-default-keybindings)
;; Make windmove work in org-mode:
(add-hook 'org-shiftup-final-hook 'windmove-up)
(add-hook 'org-shiftleft-final-hook 'windmove-left)
(add-hook 'org-shiftdown-final-hook 'windmove-down)
(add-hook 'org-shiftright-final-hook 'windmove-right)

;; --------------------------------------
;; disable word wrap
;;
(toggle-word-wrap -1)


;;
;; set frame size
;;
(setq initial-frame-alist '((width . 100)))
(setq default-frame-alist '((width . 100)))

;;
;; load theme
;;

;;(require 'color-theme-sanityinc-tomorrow)
;;(color-theme-sanityinc-tomorrow--define-theme day)

(use-package color-theme-sanityinc-tomorrow
  :config
  (load-theme 'sanityinc-tomorrow-day t)
  )

(use-package kaolin-themes
  :config
  ;;  (load-theme 'kaolin-light t)
  (kaolin-treemacs-theme)
  )


;; ------------------------------
;; Ivy config
;;
(use-package ivy
  :ensure t
  :bind (
         ("C-s" . swiper)
         ("C-x C-f" . counsel-find-file)
         ("M-x" .  counsel-M-x)
         ([f6] . ivy-resume)
         ("C-x C-b" . counsel-ibuffer)
         
         )
  :config
  (ivy-mode 1)
  )

;; -----------------------------
;; side bar for ibuffer and dired
;;
;;(global-set-key (kbd "<f8>") 'sidebar-toggle)
;;
;; it is disabled and replaced by treemacs
;;

;; (use-package dired-sidebar
;;   :bind (("C-x C-n" . dired-sidebar-toggle-sidebar));;don't toggle sidebar-toggle, ibuffer list is controlled by counsel-ibuffer
;;   :ensure t
;;   :commands (dired-sidebar-toggle-sidebar)
;;   :init
;;   (add-hook 'dired-sidebar-mode-hook
;;             (lambda ()
;;               (unless (file-remote-p default-directory)
;;                 (auto-revert-mode))))
;;   :config
;;   (push 'toggle-window-split dired-sidebar-toggle-hidden-commands)
;;   (push 'rotate-windows dired-sidebar-toggle-hidden-commands)

;;   (setq dired-sidebar-subtree-line-prefix "__")
;;   (setq dired-sidebar-theme 'none)
;;   (setq dired-sidebar-use-term-integration t)
;;   (setq dired-sidebar-use-custom-font t)
;;   (add-to-list 'dired-sidebar-special-refresh-commands 'dired-sidebar-subtree-toggle)
;; ;;  (add-to-list 'dired-sidebar-special-refresh-commands 'ibuffer-sidebar-toggle-sidebar)
;;   (add-to-list 'dired-sidebar-special-refresh-commands 'dired-sidebar-find-file)
;; ;;  (add-to-list 'dired-sidebar-special-refresh-commands 'dired-sidebar-toggle-sidebar)

;;   (defun sidebar-toggle ()
;;     "Toggle both 'dired-sidebar' and 'ibuffer-sidebar'"
;;     (interactive)
;;     (dired-sidebar-toggle-sidebar)
;;     (ibuffer-sidebar-toggle-sidebar)
;;    )
;; )

;; --------------------------------
;; set up hunspell for language check
;; TODO, hunspell not installed yet
;;
(setq ispell-program-name  "~/emax/mingw64/bin/aspell")
;;(setq ispell-personal-dictionary (concat emax-mingw64 "/share/hunspell/chp.dic"))
;;(setq ispell-dictionary  "en_US")
(global-set-key (kbd "<f9>") 'ispell-word)
(global-set-key (kbd "C-<f9>") 'flyspell-mode)
(require 'ispell) 

;; ---------------------------------
;; org mode config
;;
(use-package org
  :bind (([C-f2] . org-toggle-inline-images)
         ("C-c l" .  org-store-link)
         ("C-c k" .  org-id-store-link)
         ("C-c c" . org-capture)
         ("<f12>" . org-agenda)
         )
  :config
  (setq org-directory default-directory)
  (setq line-spacing 0.3)
  (setq org-modules '(org-habit))
  ;; {} is required to sub or supper x_{1}^{2}
  (setq org-use-sub-superscripts "{}")
 
  ;;
  ;; links config
  ;; set id for entery
  ;; set enter to open the link
  ;;
  (setq org-id-link-to-org-use-id 'create-if-interactive-and-no-custom-i)
  (setq org-return-follows-link t)

  ;;
  ;; after org load
  ;;
  (with-eval-after-load 'org
    (setq org-startup-indented t) ; Enable `org-indent-mode' by default
    (add-hook 'org-mode-hook #'visual-line-mode) ; Enable visual line, word-wrap if off for sake of Chinese words
    (add-hook 'org-mode-hook 'org-download-enable)
    (require 'org-super-agenda)
    (add-hook 'org-mode-hook 'org-super-agenda-mode)
    ) 
  (setq org-image-actual-width nil)  ; make org_attr works for image resizing

  ;;
  ;; make emphasis for Chinese words without spaces required
  ;;
  (setcar (nthcdr 0 org-emphasis-regexp-components) " \t('\"{[:nonascii:]")
  (setcar (nthcdr 1 org-emphasis-regexp-components) "- \t.,:!?;'\")}\\[[:nonascii:]")
  (org-set-emph-re 'org-emphasis-regexp-components org-emphasis-regexp-components)
  (org-element-update-syntax)
  (setq org-hide-emphasis-markers t)

  ;;
  ;; refile config
  ;;
  (setq org-refile-targets '((org-agenda-files :maxlevel . 9)))

  ;;
  ;; org-agenda config
  ;;
  
  ;; agenda view in other window
  (setq org-agenda-window-setup "other-window")
  
  (setq org-agenda-files
        (list (concat org-directory "/projects.org")
              (concat org-directory "/tickler.org")
              (concat org-directory "/inbox.org")
              ;(concat org-directory "/archive.org")
              ))
  ;;
  ;; refer to 
  ;; 1) https://blog.aaronbieber.com/2016/09/24/an-agenda-for-life-with-org-mode.html
  ;; 2) https://orgmode.org/worg/org-tutorials/org-custom-agenda-commands.html
  ;; 3) https://github.com/gjstein/emacs.d/blob/master/config/gs-org.el
  ;;   with project definition and checking helper
  ;;
  (setq org-agenda-custom-commands
        '(
          ("c" "Simple agenda view"
           ((tags "PRIORITY=\"A\""
                  ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                   (org-agenda-overriding-header "Today's :")))
            (agenda "" ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo '("DONE" "CANCELLED")))))
            (alltodo "")))

          ;; it should be triggered by org-super-agenda-mode
          ("b" "Bricks View"
           (
            (agenda "" ((org-agenda-span 'day)
                        (org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                        ))
                        ;; (org-super-agenda-groups
                        ;;  '((:name "Today"
                        ;;           :time-grid t
                        ;;           :todo "NEXT"
                        ;;           :date today
                        ;;           :scheduled today
                        ;;           :order 1)
                        ;;    ;(:discard (:anything t))
                        ;;    ))))
            (alltodo "" ((org-agenda-overriding-header "")
                         (org-super-agenda-groups
                          '((:discard (:scheduled today))
                            (:name "Next's"
                                   :todo "NEXT"
                                   :order 1
                                   :discard (:not (:todo "NEXT")))
                            ))))
            (alltodo "" ((org-agenda-overriding-header "Projects")
                         (org-super-agenda-groups
                          '((:auto-parent t)))))
            (agenda "" ((org-agenda-span 'week)
                        (org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                        ))
            ))
          ("d" "Inbox Review"
           ((tags-todo "UNPROCESSED" )))
          ))

  ;;
  ;; org capture config
  ;;
  (setq org-default-notes-file (concat org-directory "/inbox.org"))
  (setq org-capture-templates
        '(
          ("t" "Task" entry
           (file+headline "D:/Project/0UNotes/0gtd/inbox.org" "Task")
           "* TODO %^{task short desc}\n%U\n%?\n ")
          ("n" "Notes" entry
           (file+datetree "D:/Project/0UNotes/0gtd/inbox.org")
           "* %^{short desc}%^g\n%^{input the content}%?\n ")
          ("r" "Link typed References" entry
           (file+headline "D:/Project/0UNotes/0gtd/inbox.org" "References")
           "* %^{short desc}\n%x\n%?\n ")
          ))

  ;;
  ;; org babel
  ;;
  (setq org-confirm-babel-evaluate nil)
  (setq org-src-fontify-natively t)
  (setq org-src-tab-acts-natively t)

  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp .t)
     (dot . t)
     (plantuml . t)
     (org . t)))

  (setq org-plantuml-jar-path
        (concat chp-exe "/plantuml.jar"))

  (add-hook 'org-babel-after-execute-hook 'org-display-inline-images 'append)

  ;;
  ;; ox-odt export
  ;; install ox-odt from github: https://github.com/kjambunathan/org-mode-ox-odt
  ;; follow the suggested configurations
  ;; refer to ~/.emacs.d/elpa/ox-odt-/samples/masterdocument.org for syntax 
  ;; config snippet to template the headers
  ;;
  ;; after ox-odt, odt file should be loaded and saved back by libreoffice,
  ;; in order to rectified some reference links, and etc.
  ;; by "save as" option to convert to pdf and docx as required 
  ;; pandoc is not working perfectly if not load-saving odt by libreoffice
  ;; soffice --headless --convert-to pdf|docx xx.odt also requires to load-save odt first
  ;;
  ;;
  ;; (require 'yasnippet)
  ;; (yas-global-mode 1)
  ;; (yas-reload-all)
  ;; (add-hook 'org-mode #'yas-minor-mode)

  ;;   (quote (("OrgEquation" "OrgEquation"
  ;;  	    ((use-first-column-styles . t) (use-last-column-styles . t)))
  ;;  	   ("TableWithHeaderRowAndColumn" "Custom"
  ;;  	    ((use-first-row-styles . t) (use-first-column-styles . t)))
  ;;  	   ("TableWithFirstRowandLastRow" "Custom"
  ;;  	    ((use-first-row-styles . t) (use-last-row-styles . t)))))

  ;;
  ;; drag and drop pdf, msoffice document to insert a link
  ;;
  (setq dnd-protocol-alist '(
                             ("^file:.*\\(pdf\\|docx\\|doc\\|ppt\\|pptx\\|xls\\|xlsx\\)$" . dnd-insert-link)
                             ("^file:.*json$" . dnd-open-local-file)
                             dnd-protocal-alist
                             ))

  (defun dnd-insert-link (uri _action)
    (insert (concat "[[" (dnd-get-local-file-name uri)) "][" (file-name-base (dnd-get-local-file-name uri)) "]]" )
    (insert "\n")
    )

  ;;
  ;;
  ;;
  (setq org-todo-keyword-faces
        '(("TODO" . (:foreground "red" :background "black" :weight bold)) ("NEXT" . (:foreground "yellow" :background "black" :weight bold))))
  
  
  ;;end of org mode setting
  )
;; org-download config
;;
(use-package org-download
  :bind
  (([C-f1] . org-download-screenshot))

  :init
  (setq org-download-image-dir "./resources/")
  (setq org-download-method 'directory)
  (setq org-download-heading-lvl nil)
  ;; (setq org-download-timestamp nil)
  (setq org-download-image-org-width 500)
  ;;
  ;; in windows 10, use powershell script to save png file from clipboard
  ;; it replaces "convert clipboard: %s", as loading and converting is slow
  ;; 20190730 by Scott Liu
  ;;
  (setq org-download-screenshot-method "C:/Windows/System32/WindowsPowerShell/v1.0/powershell.exe -command C:/emax64/bin/Clip2PNG.ps1 %s")
  (setq org-download-screenshot-file (format "D:/tmp/Screenshot_%s.png" (format-time-string  "%Y%m%d%H-%M%S")))

  :config
  (progn
    (defun chp-org-download-annotate (link) (format "#+CAPTION: %s\n#+NAME: %s" (file-name-base link) (file-name-nondirectory link)) )
    (setq org-download-annotate-function #'chp-org-download-annotate)
    ;;(add-hook 'org-mode-hook #'org-download-enable)
    )
  )

;;(add-to-list 'org-file-apps '("\\.pptx\\" . "open %s" ))
;; (setq org-file-apps '(
;;                       ("\\.pdf\\'" . default)
;;                       (system . system)
;;                      ;; (auto-mode . emacs)
;;                       ("\\.mm\\'" . default)
;;                       ("\\.x?html?\\'" . default)
;;                       ))
(setq org-file-apps
      '(("\\.png\\'" . system)
        ("\\.docx\\'" . system)
        ("\\.doc\\'" . system)
        ("\\.ppt\\'" . system)
        ("\\.pptx\\'" . system)
        ("\\.xls\\'" . system)
        ("\\.xlsx\\'" . system)
        ("\\.mpp\\'" . system)
        ("\\.odt\\'" . system)
        ("\\.pdf\\'" . system)
        (auto-mode . emacs)))


;; youdao dic
(use-package youdao-dictionary
  :functions (posframe-show posframe-hide)
  :bind (("C-c y" . youdao-dictionary-search-at-point+)
         ("C-c Y" . youdao-dictionary-search-from-input))
  :config
  ;; Cache documents
  (setq url-automatic-caching t)

  ;; Enable Chinese word segmentation support (支持中文分词)
  ;;  (setq youdao-dictionary-use-chinese-word-segmentation t)
  )


;;
;; iconfy dired
;;
;; for speed up the icon loading performance
;; (setq inhibit-compacting-font-caches t)
;; (require 'all-the-icons-dired)
;; (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
;; (add-to-list 'all-the-icons-icon-alist '("\\.xls[xm]?$"   all-the-icons-fileicon "excel"   :face all-the-icons-orange))
;; (add-to-list 'all-the-icons-icon-alist '("\\.vsd[xm]?$"   all-the-icons-fileicon "visio"   :face all-the-icons-blue))
;; (add-to-list 'all-the-icons-icon-alist '("\\.odt$"   all-the-icons-fileicon "openoffice"   :face all-the-icons-green))

;;
;; ace-windows
;;

(use-package ace-window
    :ensure t
    :defer 1
    :bind
    (:map global-map
          ("M-o"       . ace-window))
    :config
    (set-face-attribute
     'aw-leading-char-face nil
     :foreground "deep sky blue"
     :weight 'bold
     :height 3.0)
    (set-face-attribute
     'aw-mode-line-face nil
     :inherit 'mode-line-buffer-id
     :foreground "lawn green")
    (setq aw-keys '(?a ?s ?d ?f ?j ?k ?l)
          aw-dispatch-always t
          aw-dispatch-alist
          '((?x aw-delete-window "Ace - Delete Window")
            (?c aw-swap-window "Ace - Swap Window")
            (?n aw-flip-window)
            (?v aw-split-window-vert "Ace - Split Vert Window")
            (?h aw-split-window-horz "Ace - Split Horz Window")
            (?m delete-other-windows "Ace - Maximize Window")
            (?g delete-other-windows)
            (?b balance-windows)
            (?u (lambda ()
                  (progn
                    (winner-undo)
                    (setq this-command 'winner-undo))))
            (?r winner-redo)))

    (when (package-installed-p 'hydra)
      (defhydra hydra-window-size (:color red)
        "Windows size"
        ("h" shrink-window-horizontally "shrink horizontal")
        ("j" shrink-window "shrink vertical")
        ("k" enlarge-window "enlarge vertical")
        ("l" enlarge-window-horizontally "enlarge horizontal"))
      (defhydra hydra-window-frame (:color red)
        "Frame"
        ("f" make-frame "new frame")
        ("x" delete-frame "delete frame"))
      (defhydra hydra-window-scroll (:color red)
        "Scroll other window"
        ("n" joe-scroll-other-window "scroll")
        ("p" joe-scroll-other-window-down "scroll down"))
      (add-to-list 'aw-dispatch-alist '(?w hydra-window-size/body) t)
      (add-to-list 'aw-dispatch-alist '(?o hydra-window-scroll/body) t)
      (add-to-list 'aw-dispatch-alist '(?\; hydra-window-frame/body) t))
    (ace-window-display-mode t))

;;(global-set-key (kbd "M-o") 'ace-window)

;;
;;
;; end of dotemacs

;; -----------------------------------------------------------------------------------------
;;
;; start of trial package
;;

;;
;; treeamcs
;;

(use-package treemacs
  :ensure t
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                 (if (treemacs--find-python3) 3 0)
          treemacs-deferred-git-apply-delay      0.5
          treemacs-display-in-side-window        t
          treemacs-eldoc-display                 t
          treemacs-file-event-delay              5000
          treemacs-file-follow-delay             0.2
          treemacs-follow-after-init             t
          treemacs-git-command-pipe              ""
          treemacs-goto-tag-strategy             'refetch-index
          treemacs-indentation                   2
          treemacs-indentation-string            " "
          treemacs-is-never-other-window         nil
          treemacs-max-git-entries               5000
          treemacs-missing-project-action        'ask
          treemacs-no-png-images                 nil
          treemacs-no-delete-other-windows       t
          treemacs-project-follow-cleanup        nil
          treemacs-persist-file                  (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                      'left
          treemacs-recenter-distance             0.1
          treemacs-recenter-after-file-follow    nil
          treemacs-recenter-after-tag-follow     nil
          treemacs-recenter-after-project-jump   'always
          treemacs-recenter-after-project-expand 'on-distance
          treemacs-show-cursor                   nil
          treemacs-show-hidden-files             t
          treemacs-silent-filewatch              nil
          treemacs-silent-refresh                nil
          treemacs-sorting                       'alphabetic-desc
          treemacs-space-between-root-nodes      t
          treemacs-tag-follow-cleanup            t
          treemacs-tag-follow-delay              1.5
          treemacs-width                         35)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-tag-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode t)
    (treemacs-load-theme 'kaolin)
    (pcase (cons (not (null (executable-find "git")))
                 (not (null (treemacs--find-python3))))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple))))

  ;; 
  ;; TODO ignore files and directories
  ;; TODO when treemacs toggle on, resize the frame to keep the size the current window size, when off, vice versa
  
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ([f5] . treemacs)
        ;([f8-b]   . treemacs-bookmark)
        ;("<f8> f" . treemacs-find-file)
        ;("<f8> t" . treemacs-find-tag)
        ))

(use-package treemacs-icons-dired
  :after treemacs dired
  :ensure t
  :config (treemacs-icons-dired-mode))

;;
;; Hydra
;;

(use-package hydra
  :commands defhydra
  :config

  ;; ibuffer by using hydra
  ;;
  (defhydra hydra-ibuffer-main (:color red :hint nil)
    "
 ^Navigation^ | ^Mark^        | ^Actions^        | ^View^
-^----------^-+-^----^--------+-^-------^--------+-^----^-------
  _k_:    ʌ   | _m_: mark     | _D_: delete      | _g_: refresh
 _RET_: visit | _u_: unmark   | _S_: save        | _s_: sort
  _j_:    v   | _*_: specific | _a_: all actions | _/_: filter
-^----------^-+-^----^--------+-^-------^--------+-^----^-------
"
    ("j" ibuffer-forward-line)
    ("RET" ibuffer-visit-buffer :color blue)
    ("k" ibuffer-backward-line)

    ("m" ibuffer-mark-forward)
    ("u" ibuffer-unmark-forward)
    ("*" hydra-ibuffer-mark/body :color blue)

    ("D" ibuffer-do-delete)
    ("S" ibuffer-do-save)
    ("a" hydra-ibuffer-action/body :color blue)

    ("g" ibuffer-update)
    ("s" hydra-ibuffer-sort/body :color blue)
    ("/" hydra-ibuffer-filter/body :color blue)

    ("o" ibuffer-visit-buffer-other-window "other window" :color blue)
    ("q" quit-window "quit ibuffer" :color blue)
    ("." nil "toggle hydra" :color blue))

  (defhydra hydra-ibuffer-mark (:color teal :columns 5
                                       :after-exit (hydra-ibuffer-main/body))
    "Mark"
    ("*" ibuffer-unmark-all "unmark all")
    ("M" ibuffer-mark-by-mode "mode")
    ("m" ibuffer-mark-modified-buffers "modified")
    ("u" ibuffer-mark-unsaved-buffers "unsaved")
    ("s" ibuffer-mark-special-buffers "special")
    ("r" ibuffer-mark-read-only-buffers "read-only")
    ("/" ibuffer-mark-dired-buffers "dired")
    ("e" ibuffer-mark-dissociated-buffers "dissociated")
    ("h" ibuffer-mark-help-buffers "help")
    ("z" ibuffer-mark-compressed-file-buffers "compressed")
    ("b" hydra-ibuffer-main/body "back" :color blue))

  (defhydra hydra-ibuffer-action (:color teal :columns 4
                                         :after-exit
                                         (if (eq major-mode 'ibuffer-mode)
                                             (hydra-ibuffer-main/body)))
    "Action"
    ("A" ibuffer-do-view "view")
    ("E" ibuffer-do-eval "eval")
    ("F" ibuffer-do-shell-command-file "shell-command-file")
    ("I" ibuffer-do-query-replace-regexp "query-replace-regexp")
    ("H" ibuffer-do-view-other-frame "view-other-frame")
    ("N" ibuffer-do-shell-command-pipe-replace "shell-cmd-pipe-replace")
    ("M" ibuffer-do-toggle-modified "toggle-modified")
    ("O" ibuffer-do-occur "occur")
    ("P" ibuffer-do-print "print")
    ("Q" ibuffer-do-query-replace "query-replace")
    ("R" ibuffer-do-rename-uniquely "rename-uniquely")
    ("T" ibuffer-do-toggle-read-only "toggle-read-only")
    ("U" ibuffer-do-replace-regexp "replace-regexp")
    ("V" ibuffer-do-revert "revert")
    ("W" ibuffer-do-view-and-eval "view-and-eval")
    ("X" ibuffer-do-shell-command-pipe "shell-command-pipe")
    ("b" nil "back"))

  (defhydra hydra-ibuffer-sort (:color amaranth :columns 3)
    "Sort"
    ("i" ibuffer-invert-sorting "invert")
    ("a" ibuffer-do-sort-by-alphabetic "alphabetic")
    ("v" ibuffer-do-sort-by-recency "recently used")
    ("s" ibuffer-do-sort-by-size "size")
    ("f" ibuffer-do-sort-by-filename/process "filename")
    ("m" ibuffer-do-sort-by-major-mode "mode")
    ("b" hydra-ibuffer-main/body "back" :color blue))

  (defhydra hydra-ibuffer-filter (:color amaranth :columns 4)
    "Filter"
    ("m" ibuffer-filter-by-used-mode "mode")
    ("M" ibuffer-filter-by-derived-mode "derived mode")
    ("n" ibuffer-filter-by-name "name")
    ("c" ibuffer-filter-by-content "content")
    ("e" ibuffer-filter-by-predicate "predicate")
    ("f" ibuffer-filter-by-filename "filename")
    (">" ibuffer-filter-by-size-gt "size")
    ("<" ibuffer-filter-by-size-lt "size")
    ("/" ibuffer-filter-disable "disable")
    ("b" hydra-ibuffer-main/body "back" :color blue))

  (require 'ibuffer)
  (define-key ibuffer-mode-map "." 'hydra-ibuffer-main/body)
  (add-hook 'ibuffer-hook #'hydra-ibuffer-main/body)

  ;;
  ;; dired mode by using hydra
  (defhydra hydra-dired (:hint nil :color red)
    "
_+_ mkdir          _v_iew           _m_ark             _(_ details        _i_nsert-subdir    wdired
_C_opy             _O_ view other   _U_nmark all       _)_ omit-mode      _$_ hide-subdir    C-x C-q : edit
_D_elete           _o_pen other     _u_nmark           _l_ redisplay      _w_ kill-subdir    C-c C-c : commit
_R_ename           _M_ chmod        _t_oggle           _g_ revert buf     _e_ ediff          C-c ESC : abort
_Y_ rel symlink    _G_ chgrp        _E_xtension mark   _s_ort             _=_ pdiff
_S_ymlink          ^ ^              _F_ind marked      _._ toggle hydra   \\ flyspell
_r_sync            ^ ^              ^ ^                ^ ^                _?_ summary
_z_ compress-file  _A_ find regexp
_Z_ compress       _Q_ repl regexp

T - tag prefix
"
    ("\\" dired-do-ispell)
    ("(" dired-hide-details-mode)
    (")" dired-omit-mode)
    ("+" dired-create-directory)
    ("=" diredp-ediff)         ;; smart diff
    ("?" dired-summary)
    ("$" diredp-hide-subdir-nomove)
    ("A" dired-do-find-regexp)
    ("C" dired-do-copy)        ;; Copy all marked files
    ("D" dired-do-delete)
    ("E" dired-mark-extension)
    ("e" dired-ediff-files)
    ("F" dired-do-find-marked-files)
    ("G" dired-do-chgrp)
    ("g" revert-buffer)        ;; read all directories again (refresh)
    ("i" dired-maybe-insert-subdir)
    ("l" dired-do-redisplay)   ;; relist the marked or singel directory
    ("M" dired-do-chmod)
    ("m" dired-mark)
    ("O" dired-display-file)
    ("o" dired-find-file-other-window)
    ("Q" dired-do-find-regexp-and-replace)
    ("R" dired-do-rename)
    ("r" dired-do-rsynch)
    ("S" dired-do-symlink)
    ("s" dired-sort-toggle-or-edit)
    ("t" dired-toggle-marks)
    ("U" dired-unmark-all-marks)
    ("u" dired-unmark)
    ("v" dired-view-file)      ;; q to exit, s to search, = gets line #
    ("w" dired-kill-subdir)
    ("Y" dired-do-relsymlink)
    ("z" diredp-compress-this-file)
    ("Z" dired-do-compress)
    ("q" nil)
    ("." nil :color blue))

  (require 'dired)
  (define-key dired-mode-map "." 'hydra-dired/body)

  ;; org template
  ;;
  (defhydra hydra-org-template (:color red  :hint nil)
    "
 _c_enter  _q_uote     _e_macs-lisp    _L_aTeX:
 _l_atex   _E_xample   _p_erl          _i_ndex:
 _a_scii   _v_erse     _P_erl tangled  _I_NCLUDE:
 _s_rc     _n_ote      plant_u_ml      _H_TML:
 _h_tml    ^ ^         ^ ^             _A_SCII:
"
    ("s" (hot-expand "<s"))
    ("E" (hot-expand "<e"))
    ("q" (hot-expand "<q"))
    ("v" (hot-expand "<v"))
    ("n" (hot-expand "<not"))
    ("c" (hot-expand "<c"))
    ("l" (hot-expand "<l"))
    ("h" (hot-expand "<h"))
    ("a" (hot-expand "<a"))
    ("L" (hot-expand "<L"))
    ("i" (hot-expand "<i"))
    ("e" (hot-expand "<s" "emacs-lisp"))
    ("p" (hot-expand "<s" "perl"))
    ("u" (hot-expand "<s" "plantuml :file CHANGE.png"))
    ("P" (hot-expand "<s" "perl" ":results output :exports both :shebang \"#!/usr/bin/env perl\"\n"))
    ("I" (hot-expand "<I"))
    ("H" (hot-expand "<H"))
    ("A" (hot-expand "<A"))
;    ("o" (hot-expand "<o" "org"))
    ("<" self-insert-command "ins")
    ("." nil "quit"))

  (require 'org-tempo) ; Required from org 9 onwards for old template expansion
  ;; Reset the org-template expnsion system, this is need after upgrading to org 9 for some reason
  (setq org-structure-template-alist (eval (car (get 'org-structure-template-alist 'standard-value))))
  (defun hot-expand (str &optional mod header)
    "Expand org template.

STR is a structure template string recognised by org like <s. MOD is a
string with additional parameters to add the begin line of the
structure element. HEADER string includes more parameters that are
prepended to the element after the #+HEADER: tag."
    (let (text)
      (when (region-active-p)
        (setq text (buffer-substring (region-beginning) (region-end)))
        (delete-region (region-beginning) (region-end))
        (deactivate-mark))
      (when header (insert "#+HEADER: " header) (forward-line))
      (insert str)
      (org-tempo-complete-tag)
      (when mod (insert mod) (forward-line))
      (when text (insert text))))

  (define-key org-mode-map "<"
    (lambda () (interactive)
      (if (or (region-active-p) (looking-back "^"))
          (hydra-org-template/body)
        (self-insert-command 1))))

  (eval-after-load "org"
    '(cl-pushnew
      '("not" . "note")
      org-structure-template-alist))
  ;;
  ;; TODO Org-Agenda
  ;;


  )


;;
;; centaur tabs
;;
;;  (use-package centaur-tabs
;;    :config
;;     ;; Centaur-tabs
;;    ;;(face-sepc-set
;;     ;;'centaur-tab-default
;;    ;; '((t :foregroud "blue"
;;    ;;      :backgroud "white"
;;     ;;     ))
;;    ;; 'face-defface-spec
;;    ;; )
;;   ;; (centaur-tabs-default    (:background bg0 :foreground bg0))
;;    ;;(centaur-tabs-selected   (:background bg1 :foreground fg1))
;;    ;; (centaur-tabs-unselected (:background bg0 :foreground comment))
;;    ;; (centaur-tabs-selected-modified   (:background bg1 :foreground todo))
;;    ;; (centaur-tabs-unselected-modified (:background bg0 :foreground todo))
;;    ;; (centaur-tabs-active-bar-face (:background keyword))
;;    ;; (centaur-tabs-modified-marker-selected (:inherit 'centaur-tabs-selected :foreground keyword))
;;    ;; (centaur-tabs-modified-marker-unselected (:inherit 'centaur-tabs-unselected :foreground keyword))

;;    (setq centaur-tabs-style "wave")
;;    (setq centaur-tabs-height 32)
;;    (setq centaur-tabs-set-icons t)
;;    (setq centaur-tabs-set-bar 'over)
;;    (setq centaur-tabs-set-modified-marker t)
;;    (centaur-tabs-headline-match)
;;    (centaur-tabs-inherit-tabbar-faces)
;;    (centaur-tabs-mode t)
;;    (defun centaur-tabs-buffer-groups ()
;;      "`centaur-tabs-buffer-groups' control buffers' group rules.

;;  Group centaur-tabs with mode if buffer is derived from `eshell-mode' `emacs-lisp-mode' `dired-mode' `org-mode' `magit-mode'.
;;  All buffer name start with * will group to \"Emacs\".
;;  Other buffer group by `centaur-tabs-get-group-name' with project name."
;;      (list
;;       (cond
;; 	((or (string-equal "*" (substring (buffer-name) 0 1))
;; 	     (memq major-mode '(magit-process-mode
;; 				magit-status-mode
;; 				magit-diff-mode
;; 				magit-log-mode
;; 				magit-file-mode
;; 				magit-blob-mode
;; 				magit-blame-mode
;; 				)))
;; 	 "Emacs")
;; 	((derived-mode-p 'prog-mode)
;; 	 "Editing")
;; 	((derived-mode-p 'dired-mode)
;; 	 "Dired")
;; 	((memq major-mode '(helpful-mode
;; 			    help-mode))
;; 	 "Help")
;; 	((memq major-mode '(org-mode
;; 			    org-agenda-clockreport-mode
;; 			    org-src-mode
;; 			    org-agenda-mode
;; 			    org-beamer-mode
;; 			    org-indent-mode
;; 			    org-bullets-mode
;; 			    org-cdlatex-mode
;; 			    org-agenda-log-mode
;; 			    diary-mode))
;; 	 "OrgMode")
;; 	(t
;; 	 (centaur-tabs-get-group-name (current-buffer))))))
;;    :hook
;; ;;   (dashboard-mode . centaur-tabs-local-mode)
;; ;;   (term-mode . centaur-tabs-local-mode)
;; ;;   (calendar-mode . centaur-tabs-local-mode)
;;    (org-agenda-mode . centaur-tabs-local-mode)
;;    (helpful-mode . centaur-tabs-local-mode)
;;    :bind
;;    ("C-<prior>" . centaur-tabs-backward)
;;    ("C-<next>" . centaur-tabs-forward)
;;    ("C-c t" . centaur-tabs-counsel-switch-group)
;; )
